package itv.checkout

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CheckoutTest {

    @Test
    fun When_an_item_is_scanned_it_is_added_to_the_receipt() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85)
        )

        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 1"))
        assertEquals(0.85, checkout.getReceipt()["snickers x 1"])
    }

    @Test
    fun When_an_item_does_not_have_a_price_an_exception_is_thrown() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85)
        )
        try {
            checkout.scanItem ("yorkie")
            fail("No exception thrown")
        } catch (e: ItemNotFound) {
            assertEquals("price for yorkie not found", e.message)
        }
    }

    @Test
    fun When_an_item_is_scanned_twice_the_price_is_doubled() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85)
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 2"))
        assertEquals(1.70, checkout.getReceipt()["snickers x 2"])
    }

    @Test
    fun When_an_item_is_scanned_twice_and_there_is_a_deal_for_2_then_price_is_reduced() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85),
            deals = mapOf("snickers" to Deal(2, 1.00))
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 2"))
        assertEquals(1.00, checkout.getReceipt()["snickers x 2"])
    }

    @Test
    fun When_there_is_an_odd_number_of_deal_items_spare_items_are_listed_at_full_price() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85),
            deals = mapOf("snickers" to Deal(2, 1.00))
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")
        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 2"))
        assertEquals(1.00, checkout.getReceipt()["snickers x 2"])

        assertTrue(checkout.getReceipt().containsKey("snickers x 1"))
        assertEquals(0.85, checkout.getReceipt()["snickers x 1"])
    }



    @Test
    fun When_deal_size_is_fulfilled_twice_in_a_single_basket_price_is_reduced_for_all_items() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85),
            deals = mapOf("snickers" to Deal(2, 1.00))
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")
        checkout.scanItem("snickers")
        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 4"))
        assertEquals(2.00, checkout.getReceipt()["snickers x 4"])
    }

    @Test
    fun When_scanning_items_out_of_order_deals_are_still_applied() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85, "kitkat" to 0.75),
            deals = mapOf("snickers" to Deal(2, 1.00))
        )

        checkout.scanItem("snickers")
        checkout.scanItem("kitkat")
        checkout.scanItem("snickers")

        assertTrue(checkout.getReceipt().containsKey("snickers x 2"))
        assertEquals(1.00, checkout.getReceipt()["snickers x 2"])
    }

    @Test
    fun When_getting_receipt_total_is_contains_the_full_price() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85, "kitkat" to 0.75)
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")
        checkout.scanItem("kitkat")

        assertTrue(checkout.getReceipt().containsKey("TOTAL"))
        assertEquals(2.45, checkout.getReceipt()["TOTAL"])
    }

    @Test
    fun When_getting_receipt_for_a_deal_total_is_contains_the_reduced_price() {
        val checkout = Checkout(
            prices = mapOf("snickers" to 0.85, "kitkat" to 0.75),
            deals = mapOf("snickers" to Deal(2, 1.00))
        )

        checkout.scanItem("snickers")
        checkout.scanItem("snickers")
        checkout.scanItem("kitkat")

        assertTrue(checkout.getReceipt().containsKey("TOTAL"))
        assertEquals(1.75, checkout.getReceipt()["TOTAL"])
    }

    @Test
    fun When_checkout_is_called_basket_is_cleared() {
        val checkout = Checkout(
            basket = mutableListOf("mars bar", "flake", "twirl")
        )

        checkout.checkout()

        assertEquals(1, checkout.getReceipt().size)
        assertEquals(0.00, checkout.getReceipt()["TOTAL"])
    }

}