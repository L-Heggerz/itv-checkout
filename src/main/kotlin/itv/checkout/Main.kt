package itv.checkout

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.text.NumberFormat

fun main(args: Array<String>) {

    val mapper = ObjectMapper()
    val prices: Map<String, Double> = mapper.readValue(File("src/main/resources/prices.json"), Map::class.java) as Map<String, Double>
    val deals: Map<String, Deal> =
        parseDeals(mapper.readTree(File("src/main/resources/deals.json")))
            .filter { prices.containsKey(it.key) }

    val checkout = Checkout(
        prices,
        deals
    )

    println("Welcome to HegStop")

    var input = readLine()
    while (!input.isNullOrBlank() && input != "EXIT") {
        if (input == "CHECKOUT") {
            prettyPrintReceipt(checkout.getReceipt())
            checkout.checkout()
        } else {
            try {
                checkout.scanItem(input)
                println("$input scanned")
            } catch (e: ItemNotFound) {
                println(e.message)
            }
        }
        input = readLine()!!
    }

    println("Thank you for visiting HegStop")

}

fun prettyPrintReceipt(receipt: MutableMap<String, Double>) {
    receipt.forEach { item, cost ->
        println("${item.padEnd(20, '.')}${NumberFormat.getCurrencyInstance().format(cost)}")
    }
}

fun parseDeals(tree: JsonNode): Map<String, Deal> {
    return tree
        .fieldNames()
        .asSequence()
        .map {
            it to Deal(tree[it]["count"].asInt(), tree[it]["price"].asDouble())
        }
        .toMap()
}
