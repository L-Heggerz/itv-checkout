package itv.checkout

class Checkout (
    private val prices: Map<String, Double> = emptyMap(),
    private val deals: Map<String, Deal> = emptyMap(),
    private val basket: MutableList<String> = mutableListOf()
        ) {

    fun scanItem(itemName: String) {
        if (itemName in prices.keys) {
            basket.add(itemName)
        } else {
            throw ItemNotFound(itemName)
        }
    }

    fun getReceipt(): MutableMap<String, Double> {
        val receipt : MutableMap<String, Double> = mutableMapOf()
        val itemCounts = basket.groupingBy { it }.eachCount()
        itemCounts.forEach{ (item, count) ->
            val price = prices[item]!!
            if (item in deals.keys) {
                val deal = deals[item]!!
                val numberOfItems = itemCounts[item]!!
                val numberOfDeals = numberOfItems / deal.count
                val spareItems = (numberOfItems % deal.count) * price

                if (numberOfDeals > 0) {
                    receipt["$item x ${numberOfDeals * deal.count}"] = numberOfDeals * deal.price
                }
                if (spareItems > 0) {
                    receipt["$item x ${numberOfItems % deal.count}"] = spareItems
                }
            }
            else {
                receipt["$item x $count"] = price * count
            }
        }
        receipt["TOTAL"] = receipt.values.sum()
        return receipt
    }

    fun checkout() = basket.clear()

}

data class Deal (val count: Int, val price: Double)