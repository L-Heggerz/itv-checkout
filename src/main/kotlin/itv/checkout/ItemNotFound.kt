package itv.checkout

import java.lang.RuntimeException

class ItemNotFound (
    private val itemName: String
) : RuntimeException(){

    override val message: String? = "price for $itemName not found"

}
