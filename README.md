# ITV Checkout

This is a small program I wrote for a take home test from ITV.
It mimics a checkout at a supermarket which applies deals for bulk purchases of specified items.

### Configuration

Deals and prices are specified in the resources folder. In `deals.json` and `prices.json` respectively.

### Make Commands

To build the code run `./gradlew build`

To execute the tests run `./gradlew test`

To start the app run `./gradlew run`

### Command Line Interface

The checkout logic is wrapped in a quick and dirty CLI that allows you to play around with its functionality.

After starting the program simply enter the name of an item to "scan" it.

Items without a price will be rejected.

To checkout type in `CHECKOUT` this will cause an itemized receipt to be printed and the basket cleared.

To exit the application simply enter `EXIT`
